## Music-of-the-spheres-philip-sparke-pdf

 
  
 
**DOWNLOAD ✓✓✓ [https://www.google.com/url?q=https%3A%2F%2Furluss.com%2F2tzOjr&sa=D&sntz=1&usg=AOvVaw20gmhuTXoaQQAzhpW0OjMc](https://www.google.com/url?q=https%3A%2F%2Furluss.com%2F2tzOjr&sa=D&sntz=1&usg=AOvVaw20gmhuTXoaQQAzhpW0OjMc)**

 
 
 
 
 
# Music of the Spheres by Philip Sparke: A Cosmic Concert Band Piece
 
If you are looking for a challenging and captivating concert band piece that explores the mysteries of the universe, you might want to check out **Music of the Spheres** by Philip Sparke. This piece is inspired by the ancient Pythagorean theory that the planets and the stars produce a musical harmony based on their distances from the sun and their frequencies of the musical scale. In this article, we will give you an overview of the piece, its structure, its musical themes, and where you can find a PDF score of it.
 
## Overview of Music of the Spheres
 
**Music of the Spheres** was composed by Philip Sparke in 2004 for the National Band Championships of Great Britain. Sparke is a British composer who has written many works for concert band, brass band, wind ensemble, and solo instruments. He is known for his melodic and rhythmic style, his use of modal harmonies, and his incorporation of folk and jazz elements.
 
**Music of the Spheres** is a grade 6 concert band piece that lasts about 18 minutes. It is divided into five sections that depict different aspects of the cosmos and its creation. The piece reflects Sparke's fascination with the origins of the universe and deep space in general. He uses various musical techniques to create a sense of wonder, awe, beauty, and danger.
 
## Structure and Themes of Music of the Spheres
 
The piece opens with a horn solo called *t = 0*, which represents the moment of the Big Bang when time and space were created. This is followed by a section called *The Big Bang*, which depicts the explosion of the universe from a single point. The music is loud, fast, and chaotic, with dissonant chords, syncopated rhythms, and brass fanfares.
 
The next section is called *The Lonely Planet*, which is a meditation on the unlikely set of circumstances that led to the creation of Earth as a planet that can support life. The music is slow, calm, and lyrical, with a solo flute melody that is accompanied by soft chords and percussion effects. The music gradually builds up to a climax that suggests the emergence of life on Earth.
 
The fourth section is called *Asteroids and Shooting Stars*, which depicts both the benign and dangerous objects that are flying through space and that constantly threaten our planet. The music is fast, energetic, and playful, with a theme that is passed around different instruments and sections. The music also features sudden changes in dynamics, tempo, and meter to create a sense of unpredictability and excitement.
 
The final section is called *The Unknown*, which leaves in question whether our continually expanding exploration of the universe will eventually lead to enlightenment or destruction. The music is mysterious, dark, and ominous, with a theme that is based on six notes that correspond to the six known planets from Pythagoras' theory. The music ends with a loud chord that fades away into silence.
 
## Where to Find Music of the Spheres PDF Score
 
If you are interested in playing or listening to **Music of the Spheres** by Philip Sparke, you can find a PDF score of it online at several websites. One of them is J.W. Pepper Sheet Music[^1^], where you can preview and purchase the band set and score or an additional score. Another one is Studio Music[^3^], where you can download a free PDF sample of the score. You can also visit Philip Sparke's official website[^2^], where you can listen to an audio recording of the piece performed by Tokyo Kosei Wind Orchestra.
 
**Music of the Spheres** by Philip Sparke is a remarkable concert band piece that takes you on a musical journey through space and time. It is a challenging but rewarding piece that showcases Sparke's compositional skills and his passion for astronomy. If you are looking for a cosmic concert band piece that will impress your audience and challenge your musicians, you should
 dde7e20689
 
 
