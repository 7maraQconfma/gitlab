## On Screen B2 Teachers Book Online

 
  
 
**DOWNLOAD ⚹ [https://urlcod.com/2tygkv](https://urlcod.com/2tygkv)**

 
 
 
 
 
# Review: On Screen B2+ - Teacher's Book by Jenny Dooley
 
If you are looking for a comprehensive and engaging coursebook for teaching English at an upper-intermediate level, you might want to check out On Screen B2+ - Teacher's Book by Jenny Dooley. This book is part of a series that combines active English learning with a variety of lively topics presented in themed modules.
 
The book covers all four language skills (listening, speaking, reading and writing) as well as grammar, vocabulary, notions and functions. It also provides tips and suggestions for effective classroom management, assessment and differentiation. The book comes with a Digibook app that allows you to access the audio and video materials, interactive exercises and tests online.
 
Some of the features that make this book stand out are:
 
- The use of authentic texts and realistic situations that reflect the interests and needs of modern learners.
- The integration of cross-curricular and intercultural elements that foster critical thinking and global awareness.
- The inclusion of project work, drama activities and web quests that promote collaboration and creativity.
- The provision of self-assessment tools, revision sections and progress tests that help learners monitor their own learning.

On Screen B2+ - Teacher's Book by Jenny Dooley is a valuable resource for any teacher who wants to motivate their students and help them achieve their language goals. You can find more information about this book and other titles in the series on the Express Publishing website[^1^].
  
In this review, we will look at some of the modules and activities that On Screen B2+ - Teacher's Book by Jenny Dooley offers. The book consists of 12 modules, each with a different theme and a clear learning outcome. For example, Module 1 is about "The World of Work" and aims to help learners talk about jobs and careers, write a CV and a cover letter, and prepare for a job interview. Module 6 is about "The Media" and aims to help learners express their opinions on various media sources, write a film review and a blog post, and create a podcast.
 
Each module follows a similar structure that includes the following sections:

- Lead-in: This section introduces the topic and activates the learners' prior knowledge and interest.
- Reading: This section presents one or more texts that are related to the topic and the learning outcome. The texts are authentic or semi-authentic and cover a range of genres and styles. The texts are followed by comprehension and vocabulary tasks that check the learners' understanding and help them learn new words and phrases.
- Listening: This section presents one or more audio or video recordings that are related to the topic and the learning outcome. The recordings are authentic or semi-authentic and feature different accents and registers. The recordings are followed by comprehension and vocabulary tasks that check the learners' understanding and help them learn new words and phrases.
- Speaking: This section provides opportunities for the learners to practice their speaking skills in various contexts and situations. The speaking tasks include role plays, discussions, debates, presentations and interviews. The tasks are designed to develop the learners' fluency, accuracy and confidence.
- Writing: This section guides the learners through the process of writing a specific text type that is related to the topic and the learning outcome. The writing tasks include emails, letters, reports, reviews, essays and stories. The tasks are designed to develop the learners' skills in planning, organizing, drafting, editing and proofreading.
- Grammar: This section presents and practices one or more grammar points that are relevant to the topic and the learning outcome. The grammar points are explained in clear and simple language with examples. The grammar tasks include gap-fills, multiple choice, matching, rewriting and error correction.
- Vocabulary: This section presents and practices one or more vocabulary sets that are relevant to the topic and the learning outcome. The vocabulary sets include collocations, idioms, phrasal verbs, word formation and synonyms. The vocabulary tasks include gap-fills, multiple choice, matching, crossword puzzles and word games.
- Communication: This section provides extra speaking practice for the learners in pairs or groups. The communication tasks include quizzes, surveys, puzzles, games and role plays. The tasks are designed to encourage interaction, cooperation and fun.
- Culture: This section explores one or more aspects of culture that are related to the topic and the learning outcome. The culture topics include customs, traditions, festivals, art, music, literature and history. The culture tasks include reading texts, watching videos, answering questions and doing research.
- Project: This section involves the learners in a collaborative project that is related to the topic and the learning outcome. The project tasks include brainstorming ideas, collecting information, creating a product (such as a poster, a brochure, a video or a website) and presenting it to the class.

On Screen B2+ - Teacher's Book by Jenny Dooley also includes a Grammar Reference section at the end of the book that summarizes all the grammar points covered in the modules. It also includes an Answer Key section that provides the answers to all the tasks in the book. Moreover, it includes a Digibook app that gives you access to all the audio and video materials online as well as interactive exercises and tests for each module.
 
As you can see, On Screen B2+ - Teacher's Book by Jenny Dooley is a comprehensive coursebook that covers all aspects of language learning in an engaging way. It is suitable for both classroom use and self-study. It is also aligned with the Common European Framework of Reference (CEFR) for languages at level B2+. If you want to find out more about this book or order it online, you can visit the Express Publishing website.
 dde7e20689
 
 
